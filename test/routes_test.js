const chai = require('chai');
const expect = chai.expect;
const { factorial } = require('../src/util.js');
// const { expect } = require('chai');

const http = require('chai-http');
chai.use(http);

describe('api_test_suite', () => {

	it("test_api_get_people_is_running", () => {
		chai.request('http://localhost:5001').get('/people')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
			// This is an assertation using Chai's "expect"
			// It will check if the res object is not equal to "undefined"
		})
	})

	it('test_api_get_people_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200)
			done()

		})

	})

	it('test_api_post_person_returns_400_if_no_person_name', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "James",
			age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('test_api_post_person_returns_400_if_age_is_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "James",
			age: "Twenty-Eight"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('test_fun_factorial_5!_is_120', (done) => {
		chai.request('http://localhost:5001')
		.post('/factorial')
		.type('json')
		.send({
			product: factorial(5)
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	});

	it('test_fun_factorial_1!_is_1', () => {
		chai.request('http://localhost:5001')
		.post('/one')
		.type('json')
		.send({
			product: 1
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	});

	//Test for negative numbers
	it("test_fun_factorials_neg1_is_undefined", () => {
		chai.request('http://localhost:5001')
		.post('/negative')
		.type('json')
		.send({
			product: -1
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	});


	it("test_fun_factorials_invalid_number_is_undefined", () => {
		chai.request('http://localhost:5001')
		.post('/string')
		.type('json')
		.send({
			product: "25"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	});

		})