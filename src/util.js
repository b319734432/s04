function factorial(n){
	if(typeof n !== 'number') return undefined;
	if(n < 0) return undefined;
	if(n === 0) return 1;
	if(n === 1) return 1;
	return n * factorial(n-1);
}

const names = {
	"Brandon": {
		"name": "Brandon Boyd",
		"age": 35
	},
	"Steve": {
		"name": "Style Tyler",
		"age": 56
	}
}

module.exports = {
	names: names
}