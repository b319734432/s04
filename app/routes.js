const { names } = require('../src/util.js');

module.exports = (app) => {

	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/people', (req, res) => {
		return res.send({
			people: names
		});
	});

	app.post('/person', (req, res) =>{
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error': 'Bad Request - Name has to be string'
			})
		}

		if(typeof !req.body.name !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - Name has to be a string'
			})
		}

		if(!req.body.hasOwnProperty('age')){
			return res.status(400).send({
				'error': 'Bad Request: missing required parameter AGE'
			})
		}

		if(typeof req.body.age !== 'number'){
			return res.status(400).send({
				'error': 'Bad Request - Age has to be a number'
			})
		}

	})

	app.post('/factorial', (req, res) => {
		if(n === 120) {
			return res.status(200).send({
				'result': 120
			})
		}
	})

	app.post('/one', (req, res) => {
		if(n === 1) {
			return res.status(200).send({
				'result': 1
			})
		}
	})


	app.post('/negative', (req, res) => {
		if(n < 0) {
			return res.status(400).send({
				'error': undefined
			})
		}
	})

	app.post('/string', (req, res) => {
		if(typeof req.body.name == 'string') {
			return res.status(400).send({
				'error': 'number should not be string'
			})
		}
	})
}